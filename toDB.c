#include <stdio.h>
#include <stdlib.h>
#include "sqlite3.h"

void usage(void);

static int callback(void *NotUsed, int argc, char **argv, char **azColName) {
   int i;
   for(i = 0; i<argc; i++) {
      printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
   }
   printf("\n");
   return 0;
}

int main(int argc, char ** argv){
	if(argc!=3){
	usage();
	}
	char *dbAddrss = "tank.db";
	sqlite3 *db;
	char *errMsg=0;
	int rc;
	char *insSQL;

	if((sqlite3_open(dbAddrss,&db))!=0){
		fprintf(stderr,"Cannot open DB: %s\n",sqlite3_errmsg(db));
	}

	else{
		fprintf(stdout,"Successfully connected to DB.\n");
	
	
	sprintf(insSQL,"INSERT INTO emailList (name,email) VALUES ('%s','%s');",argv[1],argv[2]);
	// insSQL="INSERT INTO emailList (name,email) VALUES ('Javad','jbafekr@r.com');";
	printf("%s\n\n",insSQL);
	rc = sqlite3_exec(db,insSQL,callback,0,&errMsg);
	if(rc!= SQLITE_OK){
		fprintf(stderr,"SQL error: %s\n", errMsg);
	}

	else{
		fprintf(stdout,"Records created successfully\n");
	}

	}
	sqlite3_close(db);

	return 0;
}

void usage(void){
	printf("./toDB [name] [email address]\n");
	exit(0);
}